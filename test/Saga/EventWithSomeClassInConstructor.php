<?php declare(strict_types = 1);

namespace Process\Test\Saga;

final class EventWithSomeClassInConstructor extends Event
{
    /** @var FirstSomeClass */
    private $obj;
    /** @var string */
    private $string;
    /** @var SecondSomeClass */
    private $secondObj;

    public function __construct(
        FirstSomeClass $obj,
        string $string,
        SecondSomeClass $secondObj
    ) {
        parent::__construct('id', 'id');
        $this->obj = $obj;
        $this->string = $string;
        $this->secondObj = $secondObj;
    }
}
