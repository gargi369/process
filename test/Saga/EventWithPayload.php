<?php declare(strict_types = 1);

namespace Process\Test\Saga;

final class EventWithPayload extends Event
{
    /** @var mixed[] */
    private $payload;

    /** @param mixed[] $payload */
    public function __construct(
        string $processId,
        string $eventId,
        array $payload
    ) {
        parent::__construct($processId, $eventId);
        $this->payload = $payload;
    }

    /** @return mixed[] */
    public function payload(): array
    {
        return $this->payload;
    }
}
