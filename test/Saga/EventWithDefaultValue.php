<?php declare(strict_types = 1);

namespace Process\Test\Saga;

final class EventWithDefaultValue extends Event
{
    public function __construct(
        string $processId = 'process-id-default',
        string $eventId = 'event-id-default'
    ) {
        parent::__construct($processId, $eventId);
    }
}
