<?php declare(strict_types = 1);

namespace Process\Test\Saga;

use Countable;
use Process\Saga;
use Process\Saga\EventStreamRepository;

final class TestSaga extends Saga implements Countable
{
    protected const LISTENING_FOR = [
        EventWhichInitializeSaga::class,
        EventWhichSagaListeningForAndHandleMethodExist::class,
        EventWhichLetDoneSaga::class,
        EventWhichSagaListeningForAndHandleMethodDoesNotExist::class,
        'FirstTimeEvent',
        'SecondTimeEvent',
    ];
    protected const EVENTS_TRIGGER_COMPENSATION = [
        EventWhichTriggerCompensation::class,
    ];

    /** @var int */
    private $eventCounter = 0;
    /** @var bool */
    public $executedSagaCloseAction = false;

    public function __construct(EventStreamRepository $states)
    {
        parent::__construct($states);
    }

    public function count(): int
    {
        return $this->eventCounter;
    }

    protected function handleEventWhichInitializeSaga(): void
    {
        $this->eventCounter++;
    }

    protected function handleEventWhichSagaListeningForAndHandleMethodExist(): void
    {
        $this->eventCounter++;
    }

    protected function handleFirstTimeEvent(): void
    {
        $this->eventCounter++;
    }

    protected function handleSecondTimeEvent(): void
    {
        $this->eventCounter++;
    }

    protected function handleEventWhichLetDoneSaga(): void
    {
        $this->eventCounter++;
    }

    protected function compensateEventWhichInitializeSaga(): void
    {
        $this->eventCounter--;
    }

    protected function compensateEventWhichSagaListeningForAndHandleMethodExist(): void
    {
        $this->eventCounter--;
    }

    protected function compensateFirstTimeEvent(): void
    {
        $this->eventCounter--;
    }

    protected function compensateSecondTimeEvent(): void
    {
        $this->eventCounter--;
    }

    protected function compensateEventWhichLetDoneSaga(): void
    {
        $this->eventCounter--;
    }

    protected function initialEvent(): string
    {
        return EventWhichInitializeSaga::class;
    }

    protected function canDone(): bool
    {
        return (bool) $this->events->reduce(
            static function (bool $acc, object $event) {
                if (EventWhichLetDoneSaga::class === get_class($event)) {
                    $acc = true;
                }

                return $acc;
            },
            false
        );
    }

    protected function closeSagaAction(): void
    {
        $this->executedSagaCloseAction = true;
    }
}
