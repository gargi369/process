<?php declare(strict_types = 1);

namespace Process\Test\Saga;

abstract class Event implements \Process\Event
{
    /** @var string */
    private $processId;
    /** @var string */
    private $eventId;

    public function __construct(string $processId, string $eventId)
    {
        $this->processId = $processId;
        $this->eventId = $eventId;
    }

    public function processId(): string
    {
        return $this->processId;
    }

    public function eventId(): string
    {
        return $this->eventId;
    }

    /** @return mixed[] */
    public function payload(): array
    {
        return [];
    }
}
