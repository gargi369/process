<?php declare(strict_types = 1);

namespace Process\Test\Saga;

final class FirstSomeClass
{
    /** @var string */
    private $a;
    /** @var int */
    private $b;
    /** @var SecondSomeClass */
    private $secondClass;

    public function __construct(string $a, int $b, SecondSomeClass $secondClass)
    {
        $this->a = $a;
        $this->b = $b;
        $this->secondClass = $secondClass;
    }
}
