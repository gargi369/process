<?php declare(strict_types = 1);

namespace Process\Test\Unit\EventBuilder;

use PHPUnit\Framework\TestCase;
use Process\EventBuilder\Reflection;
use Process\Test\Saga\EventWhichLetDoneSaga;
use Process\Test\Saga\EventWithDefaultValue;
use Process\Test\Saga\EventWithPayload;
use Process\Test\Saga\EventWithSomeClassInConstructor;
use Process\Test\Saga\FirstSomeClass;
use Process\Test\Saga\SecondSomeClass;

class ReflectionTest extends TestCase
{
    /** @var Reflection */
    private $builder;

    public function setUp(): void
    {
        $this->builder = new Reflection();
    }

    /** @test */
    public function buildEventWithTwoScalarValues(): void
    {
        $event = [
            'class' => EventWhichLetDoneSaga::class,
            'processId' => 'process-id',
            'eventId' => 'event-id',
        ];

        $event = $this->builder->build($event);

        $expectedEvent = new EventWhichLetDoneSaga(
            'process-id',
            'event-id'
        );
        self::assertEquals($expectedEvent, $event);
    }

    /** @test */
    public function buildEventWithTwoScalarValuesInPayload(): void
    {
        $event = [
            'class' => EventWhichLetDoneSaga::class,
            'payload' => [
                'processId' => 'process-id',
                'eventId' => 'event-id',
            ],
        ];

        $event = $this->builder->build($event);

        $expectedEvent = new EventWhichLetDoneSaga(
            'process-id',
            'event-id'
        );
        self::assertEquals($expectedEvent, $event);
    }

    /** @test */
    public function buildEventWithConstructorDefaultValues(): void
    {
        $event = [
            'class' => EventWithDefaultValue::class,
        ];

        $event = $this->builder->build($event);

        $expectedEvent = new EventWithDefaultValue();
        self::assertEquals($expectedEvent, $event);
    }

    /** @test */
    public function buildEventWithSomeClassInConstructor(): void
    {
        $event = [
            'class' => EventWithSomeClassInConstructor::class,
            'obj' => [
                'a' => 'a',
                'b' => 1,
                'secondClass' => [
                    'a' => 'a',
                    'b' => 'b',
                ],
            ],
            'string' => 'string',
            'secondObj' => [
                'a' => 'a',
                'b' => 'b',
            ],
        ];

        $event = $this->builder->build($event);

        $expectedEvent = new EventWithSomeClassInConstructor(
            new FirstSomeClass(
                'a',
                1,
                new SecondSomeClass('a', 'b')
            ),
            'string',
            new SecondSomeClass('a', 'b')
        );
        self::assertEquals($expectedEvent, $event);
    }

    /** @test */
    public function buildEventWithPayloadAsArray(): void
    {
        $event = [
            'class' => EventWithPayload::class,
            'processId' => 'process-id',
            'eventId' => 'event-id',
            'payload' => [
                'item-1' => [],
                'id' => 'some-id',
            ],
        ];

        $event = $this->builder->build($event);

        $expectedEvent = new EventWithPayload(
            'process-id',
            'event-id',
            [
                'item-1' => [],
                'id' => 'some-id',
            ]
        );
        self::assertEquals($expectedEvent, $event);
    }
}
