<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga;

use PHPUnit\Framework\TestCase;
use Process\Saga\EventStream;
use Process\Saga\StreamIsClosed;
use Process\Test\Saga\EventWhichSagaListeningForAndHandleMethodExist;

class EventStreamTest extends TestCase
{
    /** @test */
    public function whenICallCloseStreamThenStreamIsClosed(): void
    {
        $stream = new EventStream('some-id');

        $stream->close();

        self::assertTrue($stream->closed());
    }

    /** @test */
    public function streamIsReducedToSingleValue(): void
    {
        $event1 = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id-1'
        );
        $event2 = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id-2'
        );
        $event3 = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id-3'
        );
        $stream = new EventStream('some-id', $event1, $event2, $event3);

        $eventCountCalculatedByReduction = $stream->reduce(
            static function ($acc) {
                return ++$acc;
            },
            0
        );

        self::assertSame(3, $eventCountCalculatedByReduction);
    }

    /**
     * @test
     * @dataProvider eventsQuantity
     */
    public function streamRecordActionWhenEventOccurred(int $eventsQuantity): void
    {
        $stream = new EventStream('some-id');

        for ($i = 0; $i < $eventsQuantity; $i++) {
            $stream->recordAction(
                new EventWhichSagaListeningForAndHandleMethodExist(
                    'some-id',
                    "event-id-$i"
                )
            );
        }

        self::assertSame($eventsQuantity, $stream->reduce(
            static function ($acc) {
                return ++$acc;
            },
            0
        ));
    }

    /** @test */
    public function streamThrowsExceptionWhenTryToRecordEventInClosedStream(): void
    {
        $stream = new EventStream('some-id');
        $stream->close();

        $this->expectException(StreamIsClosed::class);

        $stream->recordAction(new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id'
        ));
    }

    /** @test */
    public function eachFunctionModifyAllEventsInStream(): void
    {
        $event1 = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id'
        );
        $event2 = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id'
        );
        $event3 = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id'
        );
        $stream = new EventStream('some-id', $event1, $event2, $event3);

        $stream->each(static function (EventWhichSagaListeningForAndHandleMethodExist $event): void {
            $event->someValue = 'a';
        });

        self::assertSame('a', $event1->someValue);
        self::assertSame('a', $event2->someValue);
        self::assertSame('a', $event3->someValue);
    }

    /** @test */
    public function filterFunctionReturnSmallerStreamByCallbackFiltration(): void
    {
        $event1 = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id-1'
        );
        $event1->someValue = 'a';
        $event2 = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id-2'
        );
        $event2->someValue = 'a';
        $event3 = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'event-id-3'
        );
        $event3->someValue = 'b';
        $stream = new EventStream('some-id', $event1, $event2, $event3);

        $filteredStream = $stream->filter(static function (EventWhichSagaListeningForAndHandleMethodExist $event) {
            return 'a' === $event->someValue;
        });

        self::assertSame(2, $filteredStream->reduce(
            static function ($acc) {
                return ++$acc;
            },
            0
        ));
    }

    /**
     * @test
     * @dataProvider eventsQuantity
     */
    public function iCanAddSameEventsToStream(int $eventsQuantity): void
    {
        $stream = new EventStream('some-id');

        for ($i = 0; $i < $eventsQuantity; $i++) {
            $stream->recordAction(
                new EventWhichSagaListeningForAndHandleMethodExist(
                    'some-id',
                    "event-id-$i"
                )
            );
        }

        self::assertSame($eventsQuantity, $stream->reduce(
            static function ($acc) {
                return ++$acc;
            },
            0
        ));
    }

    /**
     * @test
     * @dataProvider eventsQuantity
     */
    public function countOdStreamReturnEventQuantityInStream(int $eventsQuantity): void
    {
        $stream = new EventStream('some-id');

        for ($i = 0; $i < $eventsQuantity; $i++) {
            $stream->recordAction(
                new EventWhichSagaListeningForAndHandleMethodExist(
                    'some-id',
                    "event-id-$i"
                )
            );
        }

        self::assertSame($eventsQuantity, $stream->count());
    }

    /** @test */
    public function hasReturnTrueWhenEventIsInStream(): void
    {
        $event = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            "event-id"
        );
        $stream = new EventStream('some-id', $event);

        $has = $stream->has($event);

        self::assertTrue($has);
    }

    /** @test */
    public function hasReturnFalseWhenEventIsNotInStream(): void
    {
        $event = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            "event-id"
        );
        $stream = new EventStream('some-id');

        $has = $stream->has($event);

        self::assertFalse($has);
    }

    /** @return int[][] */
    public function eventsQuantity(): array
    {
        return [
            [0],
            [1],
            [5],
        ];
    }
}
