<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga;

use PHPUnit\Framework\TestCase;
use Process\Saga\EventStream;
use Process\Saga\EventStreamRepository;
use Process\Test\Saga\EventWhichInitializeSaga;
use Process\Test\Saga\TestSaga;

class SagaInitializationTest extends TestCase
{
    /** @var TestSaga */
    private $saga;
    /** @var EventStreamRepository\InMemory */
    private $streams;

    public function setUp(): void
    {
        $this->streams = new EventStreamRepository\InMemory();
        $this->saga = new TestSaga($this->streams);
    }

    /** @test */
    public function whenEventInitializingSagaOccurThenEventIsHandled(): void
    {
        $event = new EventWhichInitializeSaga(
            'some-process-id',
            'event-id'
        );

        ($this->saga)($event);

        self::assertCount(1, $this->saga);
    }

    /** @test */
    public function whenInitialEventOccurThenSagaIsInitializedWithHimProcessId(): void
    {
        $event = new EventWhichInitializeSaga(
            'some-process-id',
            'event-id'
        );

        ($this->saga)($event);

        self::assertCount(1, $this->streams);
        self::assertEquals(
            new EventStream('some-process-id', $event),
            $this->streams->get($event)
        );
    }

    /** @test */
    public function whenInitialEventIsAlreadyHandledThenEventIsDropped(): void
    {
        $event = new EventWhichInitializeSaga(
            'some-process-id',
            'event-id'
        );

        ($this->saga)($event);

        $stream = $this->streams->get($event);
        self::assertCount(1, $this->streams);
        self::assertCount(1, $stream);
    }

    /** @test */
    public function whenTwoInitialEventsWithDifferentIdOccurThenTwoProcessesAreCreated(): void
    {
        $event1 = new EventWhichInitializeSaga(
            'some-process-id-1',
            'event-id'
        );
        $event2 = new EventWhichInitializeSaga(
            'some-process-id-2',
            'event-id'
        );

        ($this->saga)($event1);
        ($this->saga)($event2);

        self::assertCount(2, $this->streams);
    }
}
