<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga;

use PHPUnit\Framework\TestCase;
use Process\Saga\EventStream;
use Process\Saga\EventStreamRepository;
use Process\Saga\SagaIsClosed;
use Process\Test\Saga\EventWhichLetDoneSaga;
use Process\Test\Saga\TestSaga;

class SagaClosingTest extends TestCase
{
    /** @var TestSaga */
    private $saga;
    /** @var EventStreamRepository\InMemory */
    private $streams;
    /** @var EventStream */
    private $events;

    public function setUp(): void
    {
        $this->events = new EventStream('some-process-id');
        $this->streams = new EventStreamRepository\InMemory($this->events);
        $this->saga = new TestSaga($this->streams);
    }

    /** @test */
    public function whenSagaFitDoneConditionThenSagaCloseActionIsExecuted(): void
    {
        $event = new EventWhichLetDoneSaga(
            'some-process-id',
            'event-id'
        );

        ($this->saga)($event);

        self::assertTrue($this->saga->executedSagaCloseAction);
    }

    /** @test */
    public function whenSagaFitDoneConditionThenStateIsClosed(): void
    {
        $event = new EventWhichLetDoneSaga(
            'some-process-id',
            'event-id'
        );

        ($this->saga)($event);

        self::assertTrue($this->events->closed());
    }

    /** @test */
    public function whenClosedSagaHandleEventThenThrowsException(): void
    {
        $event = new EventWhichLetDoneSaga(
            'some-process-id',
            'event-id'
        );
        $this->events->close();

        $this->expectException(SagaIsClosed::class);

        ($this->saga)($event);
    }
}
