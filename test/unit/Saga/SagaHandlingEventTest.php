<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga;

use PHPUnit\Framework\TestCase;
use Process\Saga\EventStream;
use Process\Saga\EventStreamRepository;
use Process\Saga\UndefinedMethodForHandleEvent;
use Process\Test\Saga\EventWhichSagaDoesNotListenFor;
use Process\Test\Saga\EventWhichSagaListeningForAndHandleMethodDoesNotExist;
use Process\Test\Saga\EventWhichSagaListeningForAndHandleMethodExist;
use Process\Test\Saga\TestSaga;
use Process\TimeEvent;

class SagaHandlingEventTest extends TestCase
{
    /** @var TestSaga */
    private $saga;
    /** @var EventStreamRepository\InMemory */
    private $streams;
    /** @var EventStream */
    private $events;

    public function setUp(): void
    {
        $this->events = new EventStream('some-process-id');
        $this->streams = new EventStreamRepository\InMemory($this->events);
        $this->saga = new TestSaga($this->streams);
    }

    /**
     * @test
     * @dataProvider eventCount
     */
    public function whenSagaIsListeningForEventAndHandleMethodExistThenEventIsHandled(int $eventCount): void
    {
        for ($i = 0; $i < $eventCount; $i++) {
            ($this->saga)(new EventWhichSagaListeningForAndHandleMethodExist(
                'some-process-id',
                "event-id-$i"
            ));
        }

        self::assertCount($eventCount, $this->saga);
    }

    /** @test */
    public function whenSagaIsListeningForEventAndHandleMethodNonExistThenThrowsException(): void
    {
        $event = new EventWhichSagaListeningForAndHandleMethodDoesNotExist(
            'some-process-id',
            'event-id'
        );

        $this->expectException(UndefinedMethodForHandleEvent::class);

        ($this->saga)($event);
    }

    /** @test */
    public function whenSagaIsListeningForTimeEventAndHandleMethodExistThenHandleEvent(): void
    {
        $firstFirstEvent = new TimeEvent(
            'some-process-id',
            'event-id-1',
            'FirstTimeEvent'
        );
        $secondTimeEvent = new TimeEvent(
            'some-process-id',
            'event-id-2',
            'SecondTimeEvent'
        );

        ($this->saga)($firstFirstEvent);
        ($this->saga)($secondTimeEvent);

        self::assertCount(2, $this->saga);
    }

    /** @test */
    public function whenEventWithProcessIdSameAsSagaOccurThenEventIsHandled(): void
    {
        $event = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-process-id',
            'event-id'
        );

        ($this->saga)($event);

        self::assertCount(1, $this->streams);
        self::assertCount(1, $this->events);
    }

    /** @test */
    public function whenEventWithProcessIdDifferentAsSagaThenEventIsDropped(): void
    {
        $event = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-different-process-id',
            'event-id'
        );

        ($this->saga)($event);

        self::assertCount(1, $this->streams);
        self::assertCount(0, $this->events);
    }

    /** @test */
    public function doNothingWhenEventWhichSagaDoesNotListenForOccurred(): void
    {
        $event = new EventWhichSagaDoesNotListenFor(
            'some-process-id',
            'event-id'
        );

        ($this->saga)($event);

        self::assertCount(0, $this->saga);
    }

    /** @test */
    public function whenDuplicatedEventOccurThenEventIsDropped(): void
    {
        $event = new EventWhichSagaListeningForAndHandleMethodExist(
            'some-process-id',
            'event-id'
        );
        ($this->saga)($event);

        ($this->saga)($event);

        self::assertCount(1, $this->streams);
        self::assertCount(1, $this->saga);
        self::assertCount(1, $this->events);
    }

    /** @test */
    public function whenDuplicatedTimeEventOccurThenEventIsDropped(): void
    {
        $event = new TimeEvent(
            'some-process-id',
            'event-id',
            'FirstTimeEvent'
        );
        ($this->saga)($event);

        ($this->saga)($event);
        ($this->saga)($event);

        self::assertCount(1, $this->streams);
        self::assertCount(1, $this->saga);
        self::assertCount(1, $this->events);
    }

    /** @return int[][] */
    public function eventCount(): array
    {
        return [
            [0],
            [1],
            [2],
            [13],
        ];
    }
}
