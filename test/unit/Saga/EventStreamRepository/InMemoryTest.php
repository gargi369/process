<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga\EventStreamRepository;

use PHPUnit\Framework\TestCase;
use Process\Saga\EventStream;
use Process\Saga\EventStreamRepository\InMemory;
use Process\Saga\SagaEventStreamAlreadyExist;
use Process\Saga\SagaEventStreamDoesNotExist;
use Process\Test\Saga\EventWhichInitializeSaga;

class InMemoryTest extends TestCase
{
    /** @test */
    public function whenAddStreamToRepositoryThenStreamIsStored(): void
    {
        $streams = new InMemory();
        $stream = new EventStream('some-process-id');

        $streams->add($stream);

        self::assertCount(1, $streams);
    }

    /** @test */
    public function whenAddExistingStreamInThenThrowsException(): void
    {
        $streams = new InMemory();
        $stream = new EventStream('some-process-id');
        $streams->add($stream);

        $this->expectException(SagaEventStreamAlreadyExist::class);

        $streams->add($stream);
    }

    /** @test */
    public function whenGetExistingStreamFromRepoThenStreamIsReturned(): void
    {
        $streams = new InMemory();
        $stream = new EventStream('some-process-id');
        $streams->add($stream);

        $downloadedStream = $streams->get(
            new EventWhichInitializeSaga(
                'some-process-id',
                'some-event-id'
            )
        );

        self::assertEquals($stream, $downloadedStream);
    }

    /** @test */
    public function whenGetNonExistingStreamFromRepoThenThrowsException(): void
    {
        $streams = new InMemory();

        $this->expectException(SagaEventStreamDoesNotExist::class);

        $streams->get(
            new EventWhichInitializeSaga(
                'some-process-id',
                'some-event-id'
            )
        );
    }
}
