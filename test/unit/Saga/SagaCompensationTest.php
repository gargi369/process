<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga;

use PHPUnit\Framework\TestCase;
use Process\Saga\EventStream;
use Process\Saga\EventStreamRepository;
use Process\Test\Saga\EventWhichSagaListeningForAndHandleMethodExist;
use Process\Test\Saga\EventWhichTriggerCompensation;
use Process\Test\Saga\TestSaga;
use Process\TimeEvent;

class SagaCompensationTest extends TestCase
{
    /** @var TestSaga */
    private $saga;
    /** @var EventStreamRepository\InMemory */
    private $streams;
    /** @var EventStream */
    private $events;

    public function setUp(): void
    {
        $this->events = new EventStream('some-process-id');
        $this->streams = new EventStreamRepository\InMemory($this->events);
        $this->saga = new TestSaga($this->streams);
    }

    /**
     * @test
     * @dataProvider eventCount
     */
    public function whenCompensationEventOccurThenCompensateAllOccurredEvents(int $eventQuantity): void
    {
        for ($i = 0; $i < $eventQuantity; $i++) {
            $event = new EventWhichSagaListeningForAndHandleMethodExist(
                'some-process-id',
                "event-id-$i"
            );
            ($this->saga)($event);
        }

        $compensationEvent = new EventWhichTriggerCompensation(
            'some-process-id',
            'event-id'
        );

        ($this->saga)($compensationEvent);

        self::assertCount(0, $this->saga);
    }

    /** @test */
    public function whenCompensationEventIsHandledThenEventIsSavedInStreamBeforeCompensationSequence(): void
    {
        $compensationEvent = new EventWhichTriggerCompensation(
            'some-process-id',
            'event-id'
        );

        ($this->saga)($compensationEvent);

        self::assertTrue($this->events->has($compensationEvent));
    }

    /** @test */
    public function compensateTimeEventWhenCompensationEventOccur(): void
    {
        $firstFirstEvent = new TimeEvent(
            'some-process-id',
            'event-id-1',
            'FirstTimeEvent'
        );
        $secondTimeEvent = new TimeEvent(
            'some-process-id',
            'event-id-2',
            'SecondTimeEvent'
        );
        $compensationEvent = new EventWhichTriggerCompensation(
            'some-process-id',
            'event-id'
        );
        ($this->saga)($firstFirstEvent);
        ($this->saga)($secondTimeEvent);

        ($this->saga)($compensationEvent);

        self::assertCount(0, $this->saga);
    }

    /** @return int[][] */
    public function eventCount(): array
    {
        return [
            [0],
            [1],
            [2],
            [13],
        ];
    }
}
