<?php declare(strict_types = 1);

namespace Process\Test\Integration\Saga\EventStreamRepository;

use PDO;
use PHPUnit\Framework\TestCase;
use Process\Event;
use Process\EventBuilder\Reflection;
use Process\Saga\EventStream;
use Process\Saga\EventStreamRepository\PDO as PDOStreams;
use Process\Test\Saga\EventWhichInitializeSaga;
use Process\Test\Saga\EventWithPayload;
use Ramsey\Uuid\Uuid;

class PDOTest extends TestCase
{
    /** @var PDO */
    private $pdo;

    public function setUp(): void
    {
        // get CI env with docker will return false, not null like declared
        // mysql -u devdb -pdevdb -h 127.0.0.1 devdb < migration/create_event_stream_table.sql
        $ci = !(bool) getenv('CI');
        $host = $ci
            ? 'database'
            : '127.0.0.1';

        $this->pdo = new PDO(
            "mysql:dbname=devdb;host=$host",
            'devdb',
            'devdb'
        );
    }

    /** @test */
    public function iCanAddEventStreamToStore(): void
    {
        $repository = new PDOStreams($this->pdo, new Reflection());
        $processId = Uuid::uuid4()->toString();
        $eventStream = $this->getEventStream($processId);

        $repository->add($eventStream);

        $stmt = $this->pdo->prepare(
            "SELECT * FROM process_event WHERE processId=:processId"
        );
        $stmt->execute([
            'processId' => $processId,
        ]);
        $rows = (array) $stmt->fetchAll();

        self::assertSame($processId, $rows[0]['processId']);
        self::assertSame(
            EventWithPayload::class,
            $rows[0]['class']
        );
        self::assertSame(
            [
                'key1' => 'v1',
                'key2' => 'v2',
            ],
            json_decode($rows[0]['payload'], true)
        );
        self::assertCount(3, $rows);
    }

    /** @test */
    public function iCanGetEventStreamFromStore(): void
    {
        $repository = new PDOStreams($this->pdo, new Reflection());
        $processId = Uuid::uuid4()->toString();
        $eventStream = $this->getEventStream($processId);
        $this->addStreamToStore($eventStream);

        $stream = $repository->get(new EventWhichInitializeSaga(
            $processId,
            Uuid::uuid4()->toString()
        ));

        self::assertCount($eventStream->count(), $stream);
    }

    private function getEventStream(string $processId): EventStream
    {
        $eventStream = new EventStream($processId);
        $payload = [
            'key1' => 'v1',
            'key2' => 'v2',
        ];
        $event1 = new EventWithPayload(
            $processId,
            Uuid::uuid4()->toString(),
            $payload
        );
        $event2 = new EventWithPayload(
            $processId,
            Uuid::uuid4()->toString(),
            $payload
        );
        $event3 = new EventWithPayload(
            $processId,
            Uuid::uuid4()->toString(),
            $payload
        );

        $eventStream->recordAction($event1);
        $eventStream->recordAction($event2);
        $eventStream->recordAction($event3);

        return $eventStream;
    }

    private function addStreamToStore(EventStream $eventStream): void
    {
        $eventStream->each(function (Event $event): void {
            $sql = "INSERT INTO process_event
VALUES (:eventId, :processId, :payload, :class)";
            $statement = $this->pdo->prepare($sql);

            $statement->execute([
                'eventId' => $event->eventId(),
                'processId' => $event->processId(),
                'payload' => json_encode($event->payload()),
                'class' => get_class($event),
            ]);
        });
    }
}
