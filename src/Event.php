<?php declare(strict_types = 1);

namespace Process;

interface Event
{
    public function processId(): string;

    public function eventId(): string;

    /** @return mixed[] */
    public function payload(): array;
}
