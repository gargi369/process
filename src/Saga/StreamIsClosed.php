<?php declare(strict_types = 1);

namespace Process\Saga;

use DomainException;

/**
 * @codeCoverageIgnore
 */
final class StreamIsClosed extends DomainException
{
}
