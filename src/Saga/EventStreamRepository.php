<?php declare(strict_types = 1);

namespace Process\Saga;

use Process\Event;

interface EventStreamRepository
{
    /** @throws SagaEventStreamAlreadyExist */
    public function add(EventStream $stream): void;
    /** @throws SagaEventStreamDoesNotExist */
    public function get(Event $event): EventStream;
}
