<?php declare(strict_types = 1);

namespace Process\Saga;

use DomainException;

/**
 * @codeCoverageIgnore
 */
final class SagaEventStreamAlreadyExist extends DomainException
{
    public static function forProcessId(string $id): self
    {
        return new self(
            sprintf('Saga for processId: %s already exist', $id)
        );
    }
}
