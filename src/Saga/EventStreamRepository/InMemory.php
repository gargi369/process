<?php declare(strict_types = 1);

namespace Process\Saga\EventStreamRepository;

use Countable;
use Process\Event;
use Process\Saga\EventStream;
use Process\Saga\EventStreamRepository;
use Process\Saga\SagaEventStreamAlreadyExist;
use Process\Saga\SagaEventStreamDoesNotExist;

final class InMemory implements EventStreamRepository, Countable
{
    /** @var EventStream[] */
    private $streams = [];

    public function __construct(EventStream ...$streams)
    {
        foreach ($streams as $stream) {
            $this->add($stream);
        }
    }

    public function add(EventStream $stream): void
    {
        if (isset($this->streams[$stream->id()])) {
            throw SagaEventStreamAlreadyExist::forProcessId($stream->id());
        }

        $this->streams[$stream->id()] = $stream;
    }

    public function get(Event $event): EventStream
    {
        if (!isset($this->streams[$event->processId()])) {
            throw SagaEventStreamDoesNotExist::withEvent($event);
        }

        return $this->streams[$event->processId()];
    }

    public function count(): int
    {
        return count($this->streams);
    }
}
