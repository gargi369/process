<?php declare(strict_types = 1);

namespace Process\Saga\EventStreamRepository;

use Process\Event;
use Process\EventBuilder;
use Process\Saga\EventStream;
use Process\Saga\EventStreamRepository;
use Process\Saga\SagaEventStreamAlreadyExist;
use Process\Saga\SagaEventStreamDoesNotExist;

final class PDO implements EventStreamRepository
{
    /** @var \PDO */
    private $pdo;
    /** @var EventBuilder  */
    private $builder;

    public function __construct(\PDO $pdo, EventBuilder $builder)
    {
        $this->pdo = $pdo;
        $this->builder = $builder;
    }

    public function __invoke(Event $event): void
    {
        $this->saveEvent($event);
    }

    /** @throws SagaEventStreamAlreadyExist */
    public function add(EventStream $stream): void
    {
        $stream->each($this);
    }

    /** @throws SagaEventStreamDoesNotExist */
    public function get(Event $event): EventStream
    {
        $stream = new EventStream($event->processId());
        $stmt = $this->pdo->prepare(
            "SELECT * FROM process_event WHERE processId=:processId"
        );
        $stmt->execute([
            'processId' => $event->processId(),
        ]);
        $events = (array) $stmt->fetchAll();

        foreach ($events as $event) {
            /** @var mixed[] $payload */
            $payload = json_decode($event['payload'], true);
            $event['payload'] = $payload;
            $event = $this->builder->build($event);
            /** @var Event $event */
            $stream->recordAction($event);
        }

        return $stream;
    }

    private function saveEvent(Event $event): void
    {
        $sql = "INSERT INTO process_event
VALUES (:eventId, :processId, :payload, :class)";
        $statement = $this->pdo->prepare($sql);

        $statement->execute([
            'eventId' => $event->eventId(),
            'processId' => $event->processId(),
            'payload' => json_encode($event->payload()),
            'class' => get_class($event),
        ]);
    }
}
