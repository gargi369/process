<?php declare(strict_types = 1);

namespace Process\Saga;

use DomainException;

/**
 * @codeCoverageIgnore
 */
final class UndefinedMethodForHandleEvent extends DomainException
{
    public static function with(string $methodName, string $className): self
    {
        return new self(
            sprintf(
                'Method %s is not defined in %s ',
                $methodName,
                $className
            )
        );
    }
}
