<?php declare(strict_types = 1);

namespace Process\Saga;

use Countable;
use Process\Event;

final class EventStream implements Countable
{
    /** @var string */
    private $id;
    /** @var Event[] */
    private $events = [];
    /** @var bool */
    private $closed = false;

    public function __construct(string $id, Event ...$events)
    {
        $this->id = $id;
        $this->events = $events;
    }

    public function recordAction(Event $event): void
    {
        if ($this->closed) {
            throw new StreamIsClosed(
                'State is closed and cant record events'
            );
        }

        $this->events[] = $event;
    }

    public function has(Event $searchedEvent): bool
    {
        return (bool) $this->reduce(
            static function ($acc, Event $event) use ($searchedEvent) {
                if ($event === $searchedEvent) {
                    $acc = $searchedEvent;
                }

                return $acc;
            }
        );
    }

    public function count(): int
    {
        return (int) $this->reduce(
            static function (int $acc) {
                return ++$acc;
            },
            0
        );
    }

    public function each(callable $callable): void
    {
        array_map($callable, $this->events);
    }

    public function filter(callable $callable): self
    {
        return new self($this->id, ...array_filter($this->events, $callable));
    }

    /**
     * @param mixed $defaultValue
     * @return mixed
     */
    public function reduce(callable $callable, $defaultValue = null)
    {
        return null === $defaultValue
            ? array_reduce($this->events, $callable)
            : array_reduce($this->events, $callable, $defaultValue);
    }

    public function close(): void
    {
        $this->closed = true;
    }

    public function closed(): bool
    {
        return $this->closed;
    }

    public function id(): string
    {
        return $this->id;
    }
}
