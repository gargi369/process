<?php declare(strict_types = 1);

namespace Process\Saga;

use DomainException;
use Process\TimeEvent;

/**
 * @codeCoverageIgnore
 */
final class SagaIsClosed extends DomainException
{
    public static function forEvent(object $event): self
    {
        return new self(
            sprintf(
                'Saga is closed but event occur and can not be handled %s',
                $event instanceof TimeEvent
                    ? $event->name()
                    : get_class($event)
            )
        );
    }
}
