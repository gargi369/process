<?php declare(strict_types = 1);

namespace Process\Saga;

use DomainException;
use Process\Event;

/**
 * @codeCoverageIgnore
 */
final class SagaEventStreamDoesNotExist extends DomainException
{
    public static function withEvent(Event $event): self
    {
        return new self(
            sprintf(
                'Saga for processId: %s does not exist, 
                event %s unhandled',
                $event->processId(),
                get_class($event)
            )
        );
    }
}
