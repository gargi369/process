<?php declare(strict_types = 1);

namespace Process\Saga;

use DomainException;

/**
 * @codeCoverageIgnore
 */
final class SagaNotFound extends DomainException
{
    public static function forCartId(string $id): self
    {
        return new self(
            'Saga for cart with id: ' . $id . ' not found.'
        );
    }
}
