<?php declare(strict_types = 1);

namespace Process;

final class TimeEvent implements Event
{
    /** @var string */
    private $name;
    /** @var string */
    private $processId;
    /** @var string */
    private $eventId;

    public function __construct(
        string $processId,
        string $eventId,
        string $name
    ) {
        $this->name = $name;
        $this->processId = $processId;
        $this->eventId = $eventId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function processId(): string
    {
        return $this->processId;
    }

    public function eventId(): string
    {
        return $this->eventId;
    }

    /** @return string[] */
    public function payload(): array
    {
        return [
            'processId' => $this->processId(),
            'eventId' => $this->eventId(),
            'name' => $this->name(),
        ];
    }
}
