<?php declare(strict_types = 1);

namespace Process;

use Exception;
use Process\Saga\EventStream;
use Process\Saga\EventStreamRepository;
use Process\Saga\SagaEventStreamAlreadyExist;
use Process\Saga\SagaEventStreamDoesNotExist;
use Process\Saga\SagaIsClosed;
use Process\Saga\UndefinedMethodForHandleEvent;
use ReflectionClass;

abstract class Saga
{
    protected const HANDLING_METHOD_PREFIX = 'handle';
    protected const COMPENSATION_METHOD_PREFIX = 'compensate';
    protected const LISTENING_FOR = [];
    protected const EVENTS_TRIGGER_COMPENSATION = [];

    /** @var EventStream */
    protected $events;
    /** @var EventStreamRepository */
    private $streams;

    abstract protected function initialEvent(): string;
    abstract protected function canDone(): bool;
    abstract protected function closeSagaAction(): void;

    protected function __construct(EventStreamRepository $states)
    {
        $this->streams = $states;
    }

    /** @throws Exception */
    public function __invoke(Event $event): void
    {
        if (!$this->listeningFor($event)) {
            return;
        }

        try {
            $this->initState($event);
        } catch (SagaEventStreamAlreadyExist $e) {
            return;
        } catch (SagaEventStreamDoesNotExist $e) {
            return;
        }

        if ($this->eventAlreadyHandled($event)) {
            return;
        }

        if ($this->isCompensationEvent($event)) {
            $this->compensation($event);

            return;
        }

        $handleMethod = $this->resolveHandleMethod($event);
        $this->checkHandleMethod($handleMethod);

        $this->events->recordAction($event);

        $this->{$handleMethod}($event);

        if (!$this->canDone()) {
            return;
        }

        $this->closeSagaAction();
        $this->events->close();
    }

    private function listeningFor(Event $event): bool
    {
        $events = array_merge(
            static::LISTENING_FOR,
            static::EVENTS_TRIGGER_COMPENSATION
        );

        if ($event instanceof TimeEvent) {
            return in_array($event->name(), $events, true);
        }

        return in_array(get_class($event), $events, true);
    }

    private function initState(Event $event): void
    {
        if ($this->isInitialEvent($event)) {
            $this->streams->add(
                new EventStream($event->processId())
            );
        }

        $this->events = $this->streams->get($event);

        if ($this->events->closed()) {
            throw SagaIsClosed::forEvent($event);
        }
    }

    private function isInitialEvent(Event $event): bool
    {
        return $this->initialEvent() === get_class($event);
    }

    private function eventAlreadyHandled(Event $occurredEvent): bool
    {
        return (bool) $this->events->reduce(
            static function (bool $acc, Event $event) use ($occurredEvent): bool {
                if ($occurredEvent->eventId() === $event->eventId()
                    && $occurredEvent->processId() === $event->processId()
                ) {
                    return true;
                }

                return $acc;
            },
            false
        );
    }

    private function isCompensationEvent(Event $event): bool
    {
        return in_array(
            get_class($event),
            static::EVENTS_TRIGGER_COMPENSATION,
            true
        );
    }

    /** @throws Exception */
    private function compensation(Event $triggeringEvent): void
    {
        $this->events->recordAction($triggeringEvent);

        $this->events->each(
            function (Event $event) use ($triggeringEvent): void {
                if ($event === $triggeringEvent) {
                    return;
                }

                $compensationMethod = $this->resolveCompensationMethod(
                    $event
                );

                $this->{$compensationMethod}($event);

                $this->events->recordAction($event);
            }
        );

        $this->events->close();
    }

    /** @throws Exception */
    private function resolveHandleMethod(Event $event): string
    {
        return $this->resolveFunctionName(
            static::HANDLING_METHOD_PREFIX,
            $event
        );
    }

    private function checkHandleMethod(string $handleMethod): void
    {
        if (!method_exists($this, $handleMethod)) {
            throw UndefinedMethodForHandleEvent::with(
                $handleMethod,
                self::class
            );
        }
    }

    /** @throws Exception */
    private function resolveCompensationMethod(Event $event): string
    {
        return $this->resolveFunctionName(
            static::COMPENSATION_METHOD_PREFIX,
            $event
        );
    }

    private function resolveFunctionName(string $prefix, Event $event): string
    {
        if ($event instanceof TimeEvent) {
            return $prefix . $event->name();
        }

        return $prefix . (new ReflectionClass($event))->getShortName();
    }
}
