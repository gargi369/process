<?php declare(strict_types = 1);

namespace Process;

interface EventBuilder
{
    /** @param mixed[] $event */
    public function build(array $event): object;
}
