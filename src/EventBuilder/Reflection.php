<?php declare(strict_types = 1);

namespace Process\EventBuilder;

use Process\EventBuilder;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;

final class Reflection implements EventBuilder
{
    /** @param mixed[] $event */
    public function build(array $event): object
    {
        $eventReflection = new ReflectionClass($event['class']);
        /** @var ReflectionMethod $constructorMethod */
        $constructorMethod = $eventReflection->getConstructor();
        $parameters = $this->collectParameters($constructorMethod, $event);

        return new $event['class'](...$parameters);
    }

    /**
     * @param mixed[] $event
     * @return mixed[]
     */
    private function collectParameters(
        ReflectionMethod $constructor,
        array $event
    ): array {
        $parameters = [];

        foreach ($constructor->getParameters() as $parameter) {
            $name = $parameter->getName();

            if (isset($event[$name])) {
                $parameters[] = $this->buildParameter(
                    $parameter,
                    $event[$name]
                );

                continue;
            }

            if (isset($event['payload'][$name])) {
                $parameters[] = $this->buildParameter(
                    $parameter,
                    $event['payload'][$name]
                );

                continue;
            }

            if ($parameter->isDefaultValueAvailable()) {
                $parameters[] = $parameter->getDefaultValue();

                continue;
            }
        }

        return $parameters;
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    private function buildParameter(ReflectionParameter $parameter, $data)
    {
        $class = $parameter->getClass();

        if (null !== $class) {
            $name = $class->getName();
            /** @var ReflectionMethod $constructor */
            $constructor = $class->getConstructor();
            $parameters = $this->collectParameters($constructor, $data);

            return new $name(...$parameters);
        }

        return $data;
    }
}
