<?php declare(strict_types = 1);

namespace Process;

use DomainException;

/**
 * @codeCoverageIgnore
 */
final class UnsupportedEvent extends DomainException
{
    public static function caughtEvent(object $event): self
    {
        return new self(
            'Unsupported event: ' . get_class($event)
        );
    }
}
