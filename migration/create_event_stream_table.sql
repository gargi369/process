CREATE TABLE process_event (
  eventId VARCHAR(36) NOT NULL PRIMARY KEY,
  processId VARCHAR(36) NOT NULL,
  payload TEXT,
  class VARCHAR(255)
);

CREATE INDEX half_of_uuid ON process_event (processId(18));